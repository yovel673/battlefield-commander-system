package com.example.gpsbattle;

import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;


import androidx.fragment.app.FragmentActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class GpsUser extends FragmentActivity implements OnMapReadyCallback
{
    public double lon = 0  , lat = 0 ;
    public  static Socket s ;
    public  static ServerSocket sS ;
    public  static InputStreamReader isr ;
    public  static BufferedReader br ;
    public  static PrintWriter pw ;
    String message =  "";
    public  static String ip ="192.168.1.107";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps_user);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ActivityCompat.requestPermissions(GpsUser.this ,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},123);
                GPStracker g = new GPStracker(getApplicationContext());
                Location l = g.getLocation();
                if (l != null)
                {
                     lat = l.getLatitude();
                     lon = l.getLongitude() ;
                }


    }
    @Override
    public void onMapReady(GoogleMap map)
    {

        Toast.makeText(getApplicationContext(), "LAT: " + lat + " \n LON: " + lon, Toast.LENGTH_LONG).show();
        map.addMarker(new MarkerOptions().position(new LatLng(lat ,lon)).title("Admin"));


        //get latlong for corners for specified city

        LatLng one = new LatLng(lat, lon);
        LatLng two = new LatLng(lat, lon);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        //add them to builder
        builder.include(one);
        builder.include(two);

        LatLngBounds bounds = builder.build();

        //get width and height to current display screen
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;

        // 20% padding
        int padding = (int) (width * 0.20);

        //set latlong bounds
        map.setLatLngBoundsForCameraTarget(bounds);

        //move camera to fill the bound to screen
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));

        //set zoom to level to current so that you won't be able to zoom out viz. move outside bounds
        map.setMinZoomPreference(map.getCameraPosition().zoom);
        message = lat + "+" + lon ;

        Task t = new Task();
        t.execute();

        Toast.makeText(getApplicationContext(), "Location Sent",Toast.LENGTH_LONG).show();

    }

    class Task extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... voids)
        {
            try {
                s = new Socket(ip, 5000);
                pw = new PrintWriter(s.getOutputStream());
                pw.write(message);
                pw.flush();
                pw.close();
                s.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return null;
        }
    }
}

