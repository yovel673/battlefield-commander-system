package com.example.gpsbattle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private EditText Name ;
    private  EditText Password;
    private Button Login ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Name = findViewById(R.id.etName);
        Password = findViewById(R.id.etPassword);
        Login  = (Button) findViewById(R.id.btnLogin);

        Login.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                validate(Name.getText().toString(), Password.getText().toString());
            }
        });

    }

    private void validate(String userName  , String userPass )
    {
        if((userName.equals("Admin") && userPass.equals("1234")) )
        {
            Intent intent = new Intent(this ,GpsUser.class);
            startActivity(intent );
        }


    }
}
